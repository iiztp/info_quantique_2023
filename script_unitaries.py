import numpy as np 
import math as math
from sim.unitaries import QuantumUnitary

#######################################################
print()
print('Ex2 Q1.a')
#######################################################
#######################################################
print('Identity Matrix')
identity = QuantumUnitary(2, np.identity(2))
print(identity)
print('Hadamard Matrix')
hadamard = QuantumUnitary(2, np.array([[1/math.sqrt(2), 1/math.sqrt(2)], [1/math.sqrt(2), -1/math.sqrt(2)]]))
print('I x H Matrix')
print(identity @ hadamard)
print('H x I Matrix')
print(hadamard @ identity)
print('H x H Matrix')
hxh = hadamard @ hadamard
print(hxh)
print('H x H == I ? ', hxh.__eq__(identity))
#######################################################

#######################################################
print()
print('Ex2 Q2.a')
#######################################################
#######################################################
U3 = QuantumUnitary(3, np.array([[0j, 1 + 0j, 0j], [math.e**((2j*math.pi)/3), 0j, 0j], [0j, 0j, math.e**((4j*math.pi)/3)]]))
#######################################################

#######################################################
print()
print('Ex2 Q2.e')
#######################################################
#######################################################
print('U3 x H')
print(U3.tensor(hadamard))
print('H x U3')
print(U3.left_tensor(hadamard))
# Bien égal à ce que l'on a calculé à la main
#######################################################

#######################################################
print()
print('Ex2 Q3.c')
#######################################################
#######################################################
X = QuantumUnitary(2, np.array([[0j, 1 + 0j], [1 + 0j, 0j]]))
print('CU_3')
print(U3.controled())
print('CH')
print(hadamard.controled())
print('CX')
print(X.controled())
#######################################################

#######################################################
print()
print('Ex2 Q3.d')
#######################################################
Z = QuantumUnitary(2, np.array([[1 + 0j, 0j], [0j, -1 + 0j]]))
res = identity.tensor(hadamard) @ Z.controled() @ identity.tensor(hadamard)
print(res)
print('Le résultat ci-dessus correspond au CNOT')
#######################################################
