from sim.states import QuantumState, QuantumQubitState
import numpy as np
import math as math

################
print()
print('Ex1 Q1.')
################
psiamp = np.array([1+0j, 0+1j, 0+0j, -2+0j, 0-3j])
psi = QuantumState(5, psiamp)
print(psi)
################

#################
print()
print('Ex1 Q1.c')
#################
print('Norme : ', psi.norm)
psi.normalize()
print('Etat après normalisation :')
print(psi)
#################

#################
print()
print('Ex1 Q2')
#################
phiamp = np.array([1/math.sqrt(3) + 0j, 1/math.sqrt(3) + 0j, 1/math.sqrt(3) + 0j])
phi = QuantumState(3, phiamp)
print(phi)
#################

#################
print()
print('Ex1 Q2.c')
#################
print(psi.tensor(phi))
#################

#################
print()
print('Ex1 Q2.d')
#################
print(phi.tensor(psi))
#################

#################
print()
print('Ex1 Q3.d')
#################
zero = QuantumQubitState(1, np.array([1 + 0j, 0 +0j]))
un = QuantumQubitState(1, np.array([0+0j, 1+0j]))
#|0>x|0>x|1>x|1>x|0>x|1>x|0>x|1>x|1>
q3d = zero.tensor(zero).tensor(un).tensor(un).tensor(zero).tensor(un).tensor(zero).tensor(un).tensor(un)
print(q3d)
#################

#################
print()
print('Ex1 Q4.a')
#################
ch_bin = '001101011'
q4a = QuantumQubitState.standard_basis_state(ch_bin)
print('q4a egal avec q3d ? : ', q4a.__eq__(q3d))
#################

#################
print()
print('Ex1 Q5.a')
#################
print('Measure on Hadamard')
hadamard = QuantumQubitState(1, np.array([1/math.sqrt(2) + 0j, 1/math.sqrt(2) +0j]))
print(hadamard.standard_basis_measurement())
print(hadamard) # Etat modifié après mesure
print('Measure on q4a')
print(q4a.standard_basis_measurement())
print(q4a)
print('Measure on q3d')
print(q3d.standard_basis_measurement())
print(q3d)
print('Quantum State on phi')
print(phi.standard_basis_measurement())
print(phi) # Etat modifié après mesure
print('Quantum state on psi')
psi = QuantumState(5, psiamp)
psi.normalize()
print(psi.standard_basis_measurement())
print(psi)
print('Quantum State on psi tensor phi normalized')
psiphi = QuantumState(5, psiamp).tensor(QuantumState(3, phiamp))
psiphi.normalize()
print(psiphi.standard_basis_measurement())
print(psiphi)
print('Quantum State on phi tensor psi normalized')
phipsi = QuantumState(3, phiamp).tensor(QuantumState(5, psiamp))
phipsi.normalize()
print(phipsi.standard_basis_measurement())
print(phipsi)
#################
