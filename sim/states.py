import numpy as np

class QuantumState:

    def __init__(self, dimension, amplitudes=None):
        assert dimension > 0, "Wrong dimension: The dimension has to be at least 1."
        self._dimension = dimension
        if amplitudes is None:
            self._amplitudes = np.zeros(dimension, dtype='complex')
            self._amplitudes[0] = 1
        else:
            assert len(amplitudes)==dimension, "Wrong dimension: The length of the array of amplitudes has to match the dimension."
            self._amplitudes = np.array(amplitudes, dtype='complex')
            assert self.is_non_zero(), "Quantum states cannot have zero norm."

    def __str__(self):
        s = ''
        for j in range(self.dimension-1):
            s += '{}|{}> + '.format(self.amplitudes[j], j)
        s+= '{}|{}>'.format(self.amplitudes[self.dimension-1], self.dimension-1)
        return s

    def __eq__(self, state2):
        return self.dimension == state2.dimension and abs(self.amplitudes - state2.amplitudes).sum() < 1e-12

    @property
    def dimension(self):
        return self._dimension

    @property
    def amplitudes(self):
        return self._amplitudes

    @property
    def conjugate(self):
        return self.amplitudes.conj()

    ###########################################################
    # Ex1 1.b
    ###########################################################
    @property
    def norm(self):
        return np.sqrt((self.conjugate @ self.amplitudes).real)
    ###########################################################

    def normalize(self):
        self._amplitudes = self.amplitudes/self.norm
        
    #####################################################################################################
    # Ex1 2.c
    #####################################################################################################
    def tensor(self, state2):
        return QuantumState(self.dimension*state2.dimension, np.kron(self.amplitudes, state2.amplitudes))
    #####################################################################################################

    def apply(self, unitary):
        assert self.dimension == unitary.dimension, "Dimensions do not match."
        self._amplitudes = unitary.matrix @ self.amplitudes

    def is_non_zero(self, precision=1e-12):
        return self.norm > precision

    def standard_basis_measurement(self):
        ################################################################
        probas = abs((self.amplitudes.conj()**2).real)
        choice = np.random.choice(self.dimension, p=probas)
        meas_result = '{}'.format(choice, self.dimension)
        self._amplitudes = np.zeros((self.dimension,), dtype = complex)
        self._amplitudes[choice] = 1 + 0j
        return meas_result
        ################################################################


class QuantumQubitState(QuantumState):

    def __init__(self, num_qubits, amplitudes=None):
        ################################################
        self._num_qubits = num_qubits
        super().__init__(2**(num_qubits), amplitudes)
        ################################################

    def __str__(self):
        s = ''
        for j in range(self.dimension-1):
            s += '{}|{:0{nq}b}> + '.format(self.amplitudes[j], j, nq=self.num_qubits)
        s+= '{}|{:0{nq}b}>'.format(self.amplitudes[self.dimension-1], self.dimension-1, nq=self.num_qubits)
        return s

    @property
    def num_qubits(self):
        return self._num_qubits

    def tensor(self, state2):
        if isinstance(state2, QuantumQubitState):
            ##########################################################################################################
            return QuantumQubitState(self.num_qubits + state2.num_qubits, np.kron(self.amplitudes, state2.amplitudes))
            ##########################################################################################################
        else:
            return super().tensor(state2)

    def standard_basis_measurement(self):
        ################################################################
        probas = abs((self.amplitudes.conj()**2).real)
        choice = np.random.choice(self.dimension, p=probas)
        meas_result = '{:0{nq}b}'.format(choice, nq=self.num_qubits)
        self._amplitudes = np.zeros((2**self.num_qubits,), dtype = complex)
        self._amplitudes[choice] = 1 + 0j
        return meas_result
        ################################################################

    @classmethod
    def standard_basis_state(cls, binary_string):
        ################################################
        zero = QuantumQubitState(1, np.array([1 + 0j, 0 +0j]))
        un = QuantumQubitState(1, np.array([0+0j, 1+0j]))
        ret = zero if binary_string[0] == '0' else un
        for i in range(1, len(binary_string)):
            bit = binary_string[i]
            if bit == '0':
                next = zero
            else:
                next = un
            ret = ret.tensor(next)
        return ret
        ################################################
