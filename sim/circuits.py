import numpy as np 
import math as math
from functools import cached_property
from sim.unitaries import QuantumUnitary

class StandardGates(QuantumUnitary):
    _gates = dict()

    def __init__(self, dimension, matrix, name):
        assert name not in StandardGates._gates, "The name {} is already used".format(name)
        self._name = name
        super().__init__(dimension, matrix)
        StandardGates._gates[name] = self

    def __str__(self):
        return '{} = \n {}'.format(self.name, self.matrix)

    @property
    def name(self):
        return self._name

    @cached_property
    def controled(self):
        quant_un = super().controled()
        return StandardGates(quant_un.dimension, quant_un.matrix, 'C'+self.name)

    @classmethod
    def gate(cls, name):
        return cls._gates[name]

    @classmethod
    def is_standard_gate(cls, name):
        return name in StandardGates._gates

    # La position est comprise entre 0 et num_qubits-1
    @classmethod
    def single_qubit_gate_in_circuit(cls, name, num_qubits, position):
        ########################################################################################
        assert(position >= 0 and position < num_qubits)
        gate = StandardGates.gate(name)
        return gate.in_context(2**(position), 2**(num_qubits - position - 1))
        ########################################################################################

    @classmethod
    def two_qubit_gate_in_circuit(cls, name, num_qubits, positions):
        ########################################################################################
        pos0, pos1 = sorted(positions)
        assert pos0 != pos1 and 0 <= pos0 < num_qubits and 0 <= pos1 < num_qubits
        gate = StandardGates.gate(name) # Porte que l'on veut ajouter dans le circuit
        sswap = StandardGates.gate('SWAP') # SWAP sur deux qubits
        swap = QuantumUnitary(2**(num_qubits), np.identity(2**num_qubits)) # I de la taille de notre dim
        for i in reversed(range(1, (pos1-pos0) + (positions[1] < positions[0]))): # On rajoute un swap si le bit de controle est a droite
            swap = sswap.in_context(2**(pos1 - i), 2**(num_qubits - 2 - pos1 + i)) @ swap
        return swap @ gate.in_context(2**pos0, 2**(num_qubits - pos0 - 2)) @ swap.inverse()
        ########################################################################################

    @classmethod
    def oracle_in_circuit(cls, name, num_qubits):
        oracle = cls.gate(name)
        assert oracle.dimension == 2**num_qubits, "{} has the wrong dimension."
        return oracle


class QuantumCircuit:

    def __init__(self, num_qubits):
        self._num_qubits = num_qubits
        self._gate_list = []

    def __str__(self):
        s = '{}\n'.format(self.num_qubits)
        for name, positions in self.gate_list:
            n = len(positions) + 1
            s += ('{} '*n+'\n').format(name, *positions)
        return s

    def append(self, gate_name, positions):
        self._gate_list.append((gate_name, positions))

    @property
    def num_qubits(self):
        return self._num_qubits

    @property
    def gate_list(self):
        return self._gate_list

    @classmethod
    def read_from_file(cls, filename):
        with open(filename, 'r') as file:
            num_qubits = int(file.readline().strip())
            circuit = QuantumCircuit(num_qubits)
            instructions = file.readlines()
            for instruction in instructions:
                comp = instruction.strip().split(' ')
                name = comp[0]
                positions = [int(j) for j in comp[1:]]
                circuit.append(name, positions)
        return circuit

    def write_to_file(self, filename):
        with open(filename, 'w') as file:
            file.write(str(self))

    def check_circuit(self):
        return all([StandardGates.is_standard_gate(g) and StandardGates.gate(g).dimension == 2**len(p) and max(p) < self.num_qubits and min(p) >= 0 for g, p in self.gate_list])

    def to_unitary(self):
        unitary = QuantumUnitary(2**self.num_qubits)
        for name, positions in self.gate_list:
            if len(positions) == 1:
                unitary = StandardGates.single_qubit_gate_in_circuit(name, self.num_qubits, positions[0]) @ unitary
            elif len(positions) == 2:
                unitary = StandardGates.two_qubit_gate_in_circuit(name, self.num_qubits, positions) @ unitary
            elif len(positions) == self.num_qubits:
                unitary = StandardGates.oracle_in_circuit(name, self.num_qubits) @ unitary
            else:
                raise Exception("Not implemented")
        return unitary


SWAP = StandardGates(4, [[1,0,0,0],[0,0,1,0],[0,1,0,0],[0,0,0,1]], 'SWAP')
##########################################################################################
# TO BE IMPLEMENTED - Ex3 1.
##########################################################################################
X = StandardGates(2, [[0j, 1 + 0j], [1 + 0j, 0j]], 'X')
Y = StandardGates(2, [[0j, -1j], [1j, 0]], 'Y')
Z = StandardGates(2, [[1+0j, 0j], [0j, -1 + 0j]], 'Z')
I2 = StandardGates(2, np.identity(2, dtype=complex), 'I2')
I4 = StandardGates(4, np.identity(4, dtype=complex), 'I4')
H = StandardGates(2, [[1/math.sqrt(2) + 0j, 1/math.sqrt(2) + 0j], [1/math.sqrt(2) + 0j, -1/math.sqrt(2) + 0j]], 'H')
CNOT = X.controled
CCNOT = CNOT.controled
##########################################################################################
OBV = StandardGates(2**10, QuantumUnitary.from_file('Oracle_BV_10.npy').matrix, 'OBV10') 
OG = StandardGates(2**10, QuantumUnitary.from_file('Oracle_Grover_10.npy').matrix, 'OG10')
