import numpy as np


class QuantumUnitary:

    def __init__(self, dimension, matrix=None):
        assert dimension > 0, "Wrong dimension: The dimension has to be at least 1."
        self._dimension = dimension
        if matrix is None:
            self._matrix = np.eye(dimension, dtype='complex')
        else:
            assert np.array(matrix).shape == (dimension, dimension), "Wrong dimension: The matrix has to match the dimension."
            self._matrix = np.array(matrix, dtype='complex')
        assert self.is_unitary(precision=1e-9), "Wrong matrix: it is not unitary."

    def __str__(self):
        return str(self.matrix)

    def __eq__(self, unitary2):
        return self.dimension == unitary2.dimension and abs(self.matrix - unitary2.matrix).sum() < 1e-12

    def __matmul__(self, unitary2):
        #################################################################################################
        assert(self.dimension == unitary2.dimension)
        return QuantumUnitary(self.dimension, self.matrix @ unitary2.matrix)
        #################################################################################################

    @property
    def dimension(self):
        return self._dimension

    @property
    def matrix(self):
        return self._matrix

    def inverse(self):
        return QuantumUnitary(self.dimension, self.matrix.transpose().conj())

    def tensor(self, unitary2):
        #################################################################################################
        return QuantumUnitary(self.dimension*unitary2.dimension, np.kron(self.matrix, unitary2.matrix))
        #################################################################################################

    def left_tensor(self, unitary2):
        #################################################################################################
        return QuantumUnitary(self.dimension*unitary2.dimension, np.kron(unitary2.matrix, self.matrix))
        #################################################################################################

    def controled(self):
        #################################################################################################
        d = self.dimension
        new_matrix = np.block([
            [np.identity(d, dtype= complex), np.zeros([d, d], dtype= complex)],
            [np.zeros([d, d], dtype= complex), self.matrix]
            ]) # If first qbit = 0 then identity else self.matrix
        return QuantumUnitary(self.dimension*2, new_matrix)
        #################################################################################################

    def in_context(self, dimension_left, dimension_right):
        identity_left = QuantumUnitary(dimension_left)
        identity_right = QuantumUnitary(dimension_right)
        return self.left_tensor(identity_left).tensor(identity_right)

    def is_unitary(self, precision=1e-12):
        return abs(self.matrix @ self.matrix.transpose().conj() - np.eye(self.dimension)).sum() < precision

    def save(self, filename):
        np.save(filename, self.matrix)

    @classmethod
    def from_file(cls, filename):
        matrix = np.load(filename)
        dimension, _ = matrix.shape
        return QuantumUnitary(dimension, matrix)

