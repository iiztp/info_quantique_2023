from sim.unitaries import QuantumUnitary
from sim.circuits import StandardGates, QuantumCircuit
import numpy as np
import math as math

#######################################################
print()
print('Ex3 Q1.b')
#######################################################
def generate_rzm(m):
    for i in range(0, m):
        Rzi = StandardGates(2, [[1 + 0j, 0j], [0j, 0 + np.exp((2*np.pi*1j)/(2**i))]], 'Rz' + str(i))
        Rzi.controled
generate_rzm(10)
#######################################################

#######################################################
print()
print('Ex3 Q3')
#######################################################
def BersteinVazirani(num_qubits, oracle_name):
    circuit = QuantumCircuit(num_qubits)
    for i in range(num_qubits):
        circuit.append('H', [i])

    circuit.append(oracle_name, range(0, num_qubits))
    
    for i in reversed(range(num_qubits)):
        circuit.append('H', [i])
    return circuit
#######################################################

#######################################################
print()
print('Ex3 Q3.a')
#######################################################
n_qubits = 10
initial_state = np.zeros((2**n_qubits, 1))
initial_state[0] = 1
circuit = BersteinVazirani(10, 'OBV10')
res = circuit.to_unitary().matrix @ initial_state
print('Res pour |0000000000> : ')
for i in range(0, len(res)):
    if abs(res[i]) > 10**(-11):
        print("{0:b}".format(i).zfill(n_qubits), ' : ', res[i])
#######################################################

#######################################################
print()
print('Ex3 Q4')
#######################################################
def QuantumFourierTransform(num_qubits):
    circuit = QuantumCircuit(num_qubits)
    for i in range(num_qubits):
        circuit.append('H', [i])
        cpt = 2
        for j in range(i, num_qubits):
            if(i == j or j == 0):
                continue
            circuit.append('CRz'+str(cpt), [j, i])
            cpt += 1

    return circuit
#######################################################

#######################################################
print()
print('Ex3 Q4.a')
#######################################################
print(QuantumFourierTransform(5))
#######################################################

#######################################################
print()
print('Ex3 Q5.c')
#######################################################
def circuitD(num_qubits):
    D = QuantumUnitary(num_qubits, np.identity(num_qubits))

    X = StandardGates.gate('X')
    XN = X
    H = StandardGates.gate('H')
    HN = H
    for i in range(num_qubits-1):
        HN = HN.tensor(H)
        XN = XN.tensor(X)
    
    Z = StandardGates.gate('Z')
    for i in range(num_qubits-1):
        Z = Z.controled

    D = HN @ XN @ Z @ XN @ HN
    return StandardGates(2**(num_qubits), D.matrix, 'D' + str(num_qubits))

def Grover(num_qubits, oracle_name):
    circuit = QuantumCircuit(num_qubits)
    for i in range(num_qubits):
        circuit.append('H', [i])

    for i in range(int((np.pi * math.sqrt(2**(num_qubits)))/4)):
        circuit.append(oracle_name, range(0, num_qubits))
        circuit.append('D'+str(num_qubits), range(0, num_qubits))
    
    return circuit
#######################################################

#######################################################
print()
print('Ex3 Q5.d')
#######################################################
n_qubits = 10
initial_state = np.zeros((2**n_qubits, 1))
initial_state[0] = 1
circuitD(n_qubits)
circuit = Grover(n_qubits, 'OG10')
circmat = circuit.to_unitary().matrix
res = circmat @ initial_state
print('Res pour |0000000000> : ')
for i in range(0, len(res)):
    if abs(res[i]) > 10**(-1):
        print("{0:b}".format(i).zfill(n_qubits), ' : ', res[i])
#######################################################
